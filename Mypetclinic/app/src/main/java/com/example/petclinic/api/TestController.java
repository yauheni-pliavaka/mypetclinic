package com.example.petclinic.api;

import com.example.petclinic.config.Runner;
import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.OwnerReceipt;
import com.example.petclinic.entity.User;
import com.example.petclinic.repository.OwnerReceiptRepository;
import com.example.petclinic.repository.OwnerRepository;
import com.example.petclinic.repository.UserRepository;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.specification.filter.OwnerFilter;
import com.example.system.entity.SystemOption;
import com.example.system.repository.SystemRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/")
@RestController
public class TestController {

    private final OwnerReceiptRepository ownerReceiptRepository;
    private final SystemRepository systemRepository;
    private final OwnerRepository ownerRepository;
    private final OwnerService ownerService;

    private final Runner runner;

    @GetMapping
    @Transactional
    public String test() {
        User user = runner.getUser();
        user.getPermissions().forEach(p -> System.out.println(p.getName()));
        return user.getLogin();
    }

    @GetMapping("/view")
    public List<OwnerReceipt> getView() {
        return ownerReceiptRepository.findByPayerNameContainsIgnoreCase("a");
    }

    @GetMapping("/system")
    public List<SystemOption> getSystem() {
        return systemRepository.findAll();
    }

    @ApiOperation(value = "Get all owners", response = Owner.class, responseContainer = "List")
    @GetMapping("/owners")
    public List<Owner> getOwners() {
        return ownerRepository.findAll();
    }

    @ApiOperation(value = "Find owners", response = Owner.class, responseContainer = "List")
    @PutMapping("/owners/find")
    public List<OwnerDto> findOwners(@ApiParam(value = "Search example") @RequestBody OwnerDto ownerDto) {
        return ownerService.findOwner(ownerDto);
    }

    @ApiOperation(value = "Search owners", response = Owner.class, responseContainer = "Page")
    @GetMapping("/owners/search")
    public Page<OwnerDto> search(@RequestParam(required = false) String term,
                                 @RequestParam(required = false) Integer pageNumber) {
        OwnerFilter ownerFilter = OwnerFilter.builder()
                .pageNumber(pageNumber)
                .term(term)
                .build();
        return ownerService.search(ownerFilter);
    }
}
