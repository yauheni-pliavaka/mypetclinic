package com.example.petclinic.config;

import com.example.petclinic.excepcion.ConfigurationException;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.SQLException;

@Slf4j
public class ManagerHikariDataSource extends HikariDataSource {

    @PostConstruct
    public void checkConnection() {
        try {
            log.info("Check db connection");
            Connection connection = this.getConnection();
            connection.close();
        } catch (SQLException e) {
            log.error("Cannot connect to db: pollName = {}, url = {}, login = {}, password = {}", this.getPoolName(),
                    this.getJdbcUrl(), this.getUsername(), this.getPassword());
            throw new ConfigurationException("Fatal db connection error", e);
        }
    }
}
