package com.example.petclinic.specification;

import com.example.petclinic.entity.ContactDetails_;
import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.Owner_;
import com.example.petclinic.specification.filter.OwnerFilter;
import com.example.petclinic.specification.filter.SpecBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public interface OwnerSpecification extends Specification<Owner> {

    static OwnerSpecification haveFirstName(String value) {
        return (root, cq, cb) -> cb.equal(cb.lower(root.get(Owner_.FIRST_NAME)), value.toLowerCase());
    }

    static OwnerSpecification firstNameLike(String value) {
        return (root, cq, cb) -> cb.like(cb.lower(root.get(Owner_.FIRST_NAME)), "%" + value.toLowerCase() + "%");
    }

    static OwnerSpecification haveLastName(String value) {
        return (root, cq, cb) -> cb.equal(cb.lower(root.get(Owner_.LAST_NAME)), value.toLowerCase());
    }

    static OwnerSpecification lastNameLike(String value) {
        return (root, cq, cb) -> cb.like(cb.lower(root.get(Owner_.LAST_NAME)), "%" + value.toLowerCase() + "%");
    }


    static OwnerSpecification cityLike(String value) {
        return (root, cq, cb) -> cb.like(cb.lower(root.get(ContactDetails_.CITY)), "%" + value.toLowerCase() + "%");
    }

    static OwnerSpecification streetLike(String value) {
        return (root, cq, cb) -> cb.like(cb.lower(root.get(ContactDetails_.STREET)), "%" + value.toLowerCase() + "%");
    }


    static Specification<Owner> findByTerm(String term) {
        List<Specification<Owner>> specList = new ArrayList<>();
        specList.add(firstNameLike(term));
        specList.add(lastNameLike(term));
        specList.add(cityLike(term));
        specList.add(streetLike(term));

        return SpecificationComposer.compose(specList, Predicate.BooleanOperator.OR);
    }


    static OwnerSpecification.Builder builder() {
        return new OwnerSpecification.Builder();
    }

    class Builder extends SpecBuilder<Owner, OwnerFilter> {

        @Override
        public Specification<Owner> build() {
            List<Specification<Owner>> specList = new ArrayList<>();

            if (StringUtils.isNotEmpty(filter.getFirstName())) {
                specList.add(haveFirstName(filter.getFirstName()));
            }

            if (StringUtils.isNotEmpty(filter.getLastName())) {
                specList.add(haveLastName(filter.getLastName()));
            }

            if (StringUtils.isNotEmpty(filter.getTerm())) {
                specList.add(findByTerm(filter.getTerm()));
            }

            return SpecificationComposer.compose(specList);
        }
    }
}
