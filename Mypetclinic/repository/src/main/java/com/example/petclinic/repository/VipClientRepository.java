package com.example.petclinic.repository;

import com.example.petclinic.entity.VipClient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VipClientRepository extends JpaRepository<VipClient, Long> {
}
