package com.example.system.repository;

import com.example.system.entity.SystemOption;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SystemRepository extends JpaRepository<SystemOption, String> {
}
