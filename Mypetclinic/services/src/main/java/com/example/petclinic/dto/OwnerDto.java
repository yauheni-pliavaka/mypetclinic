package com.example.petclinic.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OwnerDto {

    private Long id;
    private String phone;
    private String city;
    private String street;
    private String building;
    private String firstName;
    private String lastName;
    private String fullName;

    private OffsetDateTime createdAt;
    private String createdBy;
    private OffsetDateTime lastModifiedAt;
    private String lastModifiedBy;
}
