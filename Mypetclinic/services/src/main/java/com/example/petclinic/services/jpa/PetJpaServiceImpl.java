package com.example.petclinic.services.jpa;

import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.Pet;
import com.example.petclinic.repository.PetRepository;
import com.example.petclinic.services.PetService;
import com.example.petclinic.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Transactional(readOnly = true)
@JpaImplementation
public class PetJpaServiceImpl extends AbstractJpaService<Pet, Long> implements PetService {

    private final PetRepository petRepository;

    @Override
    public JpaRepository<Pet,Long> getRepository() {
        return petRepository;
    }

    @Override
    public Pet findByOwner(Owner owner) {
        throw new UnsupportedOperationException();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updatePetName(Long petId, String petName) {
        Pet pet = petRepository.findById(petId).orElseThrow();
        pet.setName(petName);
    }
}
