package com.example.petclinic.services;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.entity.Owner;
import com.example.petclinic.specification.filter.OwnerFilter;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;

public interface OwnerService extends CrudService<Owner, Long> {

    Collection<Owner> findByName(String name);

    default List<OwnerDto> findOwner(OwnerDto ownerDto) {
        throw new UnsupportedOperationException();
    }

    default Page<OwnerDto> search(OwnerFilter ownerFilter) {
        throw new UnsupportedOperationException();
    }
}
