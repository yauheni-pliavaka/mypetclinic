package com.example.petclinic.services.jpa;

import com.example.petclinic.dto.OwnerDto;
import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.Pet;
import com.example.petclinic.mapper.OwnerMapper;
import com.example.petclinic.repository.OwnerRepository;
import com.example.petclinic.repository.PetRepository;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.services.PetService;
import com.example.petclinic.services.config.JpaImplementation;
import com.example.petclinic.specification.SearchableRepository;
import com.example.petclinic.specification.SearchableService;
import com.example.petclinic.specification.filter.OwnerFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@JpaImplementation
public class OwnerJpaServiceImpl extends AbstractJpaService<Owner, Long> implements OwnerService, SearchableService<Owner> {

    private final OwnerRepository ownerRepository;
    private final PetService petService;
    private final PetRepository petRepository;
    private final OwnerMapper mapper;

    @Override
    public JpaRepository<Owner, Long> getRepository() {
        return ownerRepository;
    }

    @Override
    public SearchableRepository<Owner, ?> getSearchRepository() {
        return ownerRepository;
    }

    @Override
    public Collection<Owner> findByName(String name) {
        throw new UnsupportedOperationException();
    }

    public void readUserAndPet() {
        List<Owner> owners = ownerRepository.findAll();
        for (Owner owner : owners) {
            log.warn("Owner name {}", owner.getFullName());
            for (Pet pet : owner.getPets()) {
                log.warn("Pet name {}", pet.getName());
            }
        }
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void updateOwnerAndPet(Long id, String lastName, String petName) {
        Owner owner = ownerRepository.findById(id).orElseThrow();
        owner.setLastName(lastName);

        if (CollectionUtils.isNotEmpty(owner.getPets())) {
            log.info("Previous pet name: {}", owner.getPets().get(0).getName());

            updatePetName(owner.getPets().get(0).getId(), petName);
        }


//        ownerRepository.save(owner); //так как транзакция - изменения сразу попадают в бд
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updatePetName(Long petId, String petName) {
        try {
            Pet pet = petRepository.findById(petId).orElseThrow();
            pet.setName(petName);
        } catch (Exception exception) {
            throw new RuntimeException();
        }
    }

    @Override
    public List<OwnerDto> findOwner(OwnerDto ownerDto) {
        ExampleMatcher caseInsensitiveExMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
        Example<Owner> example = Example.of(mapper.map(ownerDto), caseInsensitiveExMatcher);
        return ownerRepository.findAll(example).stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    public Page<OwnerDto> search(OwnerFilter ownerFilter) {
        return searchPage(ownerFilter).map(mapper::mapToDto);
    }
}
