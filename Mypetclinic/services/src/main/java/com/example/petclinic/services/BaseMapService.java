package com.example.petclinic.services;

import java.util.Map;

public interface BaseMapService<T, ID> {

    Map<ID, T> getResources();
}
