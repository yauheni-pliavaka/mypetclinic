package com.example.system.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "SYSTEM_OPTION")
public class SystemOption {

    @Id
    private String id;

    private String value;
}
