package com.example.petclinic.entity;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;


@Setter
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class ProductPK implements Serializable {

    private String code;
    private String codePart;
}
