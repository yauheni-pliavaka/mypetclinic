package com.example.petclinic.entity;

import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Table(name = "vet_visit")
@Entity
public class Visit {

    @Id
    @GeneratedValue
    private UUID id;
    private int version;
    @Column(name = "visit_time")
    private OffsetDateTime time;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "visit_note_ID")
    private Notes notes;

    @ManyToOne
    private Pet pet;

    @ManyToOne
    private Vet vet;

    @OneToOne
    private Receipt receipt;
}
