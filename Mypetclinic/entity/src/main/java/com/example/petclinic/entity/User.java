package com.example.petclinic.entity;

import com.example.petclinic.converter.RoleConverter;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String login;
    private String password;

    @Convert(converter = RoleConverter.class)
    private Role role;

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.LAZY)
    @JoinTable(name = "user_perm",
            joinColumns = @JoinColumn(name = "usr_id"),
            inverseJoinColumns = @JoinColumn(name = "perm_id")
    )
    private Set<Permission> permissions;
    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<Filial> filials;

    @Embedded
    private ContactDetails contactDetails;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name = "stree2")),
            @AttributeOverride(name = "building", column = @Column(name = "building2")),
            @AttributeOverride(name = "city", column = @Column(name = "city2")),
            @AttributeOverride(name = "phone", column = @Column(name = "phone2"))
    })
    private ContactDetails additional;
}
