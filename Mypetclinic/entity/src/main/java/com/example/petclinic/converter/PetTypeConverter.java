package com.example.petclinic.converter;

import com.example.petclinic.entity.PetType;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class PetTypeConverter implements AttributeConverter<PetType, String> {

    @Override
    public String convertToDatabaseColumn(PetType attribute) {
        return attribute.getValue();
    }

    @Override
    public PetType convertToEntityAttribute(String dbData) {
        return PetType.getByValue(dbData);
    }
}
